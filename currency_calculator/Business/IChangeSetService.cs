﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace currency_calculator.Business
{
    public interface IChangeSetService
    {
        Task<Dictionary<string, Rates>> Load();
    }
}