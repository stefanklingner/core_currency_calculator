﻿namespace currency_calculator.Business
{
    public interface IChangeRates
    {
        decimal Eur { get; set; }
        decimal Usd { get; set; }
        decimal Btc { get; set; }
        decimal Eth { get; set; }
        decimal Ltc { get; set; }
        decimal Iota { get; set; }
    }
}