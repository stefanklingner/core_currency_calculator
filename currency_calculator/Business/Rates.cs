﻿namespace currency_calculator.Business
{
    public class Rates : IChangeRates
    {
        public decimal Eur { get; set; }
        public decimal Usd { get; set; }
        public decimal Btc { get; set; }
        public decimal Eth { get; set; }
        public decimal Ltc { get; set; }
        public decimal Iota { get; set; }
    }
}