﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace currency_calculator.Business
{
    public interface IChangeSet
    {
        Task<Dictionary<string, Rates>> Get();
    }

    public class ChangeSet : IChangeSet
    {              
      
        private readonly IChangeSetService _changeSetService;
        private static readonly Stopwatch Stopwatch = new Stopwatch();
        private Dictionary<string, Rates> _changeRates = new Dictionary<string, Rates>();

        public ChangeSet(IChangeSetService changeSetService)
        {
            _changeSetService = changeSetService ?? throw new ArgumentNullException(nameof(changeSetService));
        }
        
        public async Task<Dictionary<string, Rates>> Get()
        {
            var alreadyLoaded = false;
            if (!Stopwatch.IsRunning)
            {
                Stopwatch.Start();
                _changeRates = await _changeSetService.Load();
                alreadyLoaded = true;
            }
            
            if (!alreadyLoaded && Stopwatch.ElapsedMilliseconds >= 1000)
            {
                _changeRates = await _changeSetService.Load();
                Stopwatch.Restart();
            }
                    
            return new Dictionary<string, Rates>(_changeRates);
        }
    }
   
}