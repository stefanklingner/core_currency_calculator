﻿namespace currency_calculator.Business
{
    public interface ICalculator
    {
        IChangeRates Get(string currency, decimal value);
    }
}