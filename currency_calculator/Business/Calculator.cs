﻿using System;
using Newtonsoft.Json;

namespace currency_calculator.Business
{
    public class Calculator : ICalculator
    {
        private readonly IChangeSet _changeSet;
        
        public Calculator(IChangeSet changeSet)
        {
            _changeSet = changeSet ?? throw new ArgumentNullException(nameof(changeSet));
        }

        public IChangeRates Get(string currency, decimal value)
        {
            var rates = _changeSet.Get().Result[currency];
            return new Rates()
            {
                Btc = rates.Btc * value,
                Eth = rates.Eth * value,
                Eur = rates.Eur * value,
                Iota = rates.Iota * value,
                Ltc = rates.Ltc * value,
                Usd = rates.Usd * value
            };
        }
    }
}