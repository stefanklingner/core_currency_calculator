﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace currency_calculator.Business
{
    public class ChangeSetService : IChangeSetService 
    {
        public async Task<Dictionary<string, Rates>> Load()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    const string address = "https://min-api.cryptocompare.com/data/pricemulti?fsyms=EUR,USD,BTC,ETH,LTC,IOTA&tsyms=EUR,USD,BTC,ETH,LTC,IOTA&extraParams=currency_calculator";
                    var response = await client.GetAsync(address);
                    response.EnsureSuccessStatusCode();
                    var stringResult = await response.Content.ReadAsStringAsync();
                    
                    var rates = JsonConvert.DeserializeObject<Dictionary<string, Rates>>(stringResult);

                    return rates;
                }
                catch (HttpRequestException httpRequestException)
                {
                    Debug.Print($"Error getting weather from OpenWeather: {httpRequestException.Message}");
                    throw;
                }
            }
        }
    }
}