﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using currency_calculator.Business;
using Microsoft.AspNetCore.Mvc;

namespace currency_calculator.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly ICalculator _calculator;
        
        public ValuesController(ICalculator calculator)
        {
            _calculator = calculator ?? throw new ArgumentNullException(nameof(calculator));
            
        }
        
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] {"value1", "value2"};
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // GET api/values/5
        [HttpGet("{currency, amount}")]
        public ActionResult Get(string currency, decimal amount)
        {
            try
            {
                var res = _calculator.Get(currency.ToUpper(), amount);
                return Json(res);
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }

            return Json(new {success = "false", message = "error bla"});
        }
        
        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}