using System.Collections.Generic;
using System.Threading.Tasks;
using currency_calculator.Business;
using FluentAssertions;
using Xunit;

namespace CurrencyCalculatorTest
{
    public class RatesService : IChangeSetService
    {
        public Task<Dictionary<string, Rates>> Load()
        {
            var eur = new Rates()
            {
                Btc = 8000m,
                Eth = 200m,
                Iota = 0.42m,
                Ltc = 120m,
                Usd = 1.2m,
                Eur = 1m
            };
            var usd = new Rates()
            {
                Btc = 8200m,
                Eth = 210m,
                Iota = 0.49m,
                Ltc = 130m,
                Usd = 1m,
                Eur = 0.9m
            };

            return Task.Run(() =>
            {
                var result = new Dictionary<string, Rates> {{"usd", usd}, {"eur", eur}};
                return result;
            });
        }
    }

    public class CalculatorTest
    {

        [Fact]
        public void Test1()
        {
            //Arrange
            var rates = new ChangeSet(new RatesService());
            var c = new Calculator(rates);

            //Act
            var res = c.Get("eur", 3);

            //Assert
            res.Btc.Equals(24000m);
            res.Eth.Equals(600m);
        }

        [Fact]
        public void IntegrationTest()
        {
            //Arrange
            var rates = new ChangeSet(new ChangeSetService());
            var c = new Calculator(rates);

            //Act
            var res = c.Get("Eur", 3);

            //Assert
            res.Btc.Should().NotBe(null);
            //res.Eth.Equals(600m);

        }
        
    }
    
}